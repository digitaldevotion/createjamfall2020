﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLightColor : MonoBehaviour
{
    public List<Light> shipLights;
    public List<MeshRenderer> shipMaterials;
    [ColorUsage(true, true)]
    public Color emissionColor;
    [ColorUsage(true, true)]
    public Color lightColor;
    public bool pulseLight;
    public float speed;
    public float lightIntensity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (pulseLight)
        {
            for (int i = 0; i < shipLights.Count; i++)
            {
                shipLights[i].intensity = Mathf.PingPong(Time.time * speed, lightIntensity);
            }
        }
    }

    // Update is called once per frame
    [ContextMenu("GetLights")]

    public void GetLights()
    {
        shipLights.Clear();
        shipLights.AddRange(GetComponentsInChildren<Light>());
    }
    [ContextMenu("GetMaterials")]

    public void GetMaterials()
    {
        shipMaterials.Clear();
      //  shipMaterials.AddRange(GetComponentsInChildren<MeshRenderer>());
    }
    [ContextMenu("ChangeLights")]
    public void ChangeLightColors()
    {
        pulseLight = true;
        for (int i = 0; i < shipLights.Count; i++)
        {
            shipLights[i].color = lightColor;
        }
    }
    [ContextMenu("ChangeEmission")]
    public void ChangeEmissionColors()
    {
        for (int i = 0; i < shipMaterials.Count; i++)
        {
          shipMaterials[i].materials[1].SetColor("_EmissiveColor", emissionColor);

        }
    }
}
