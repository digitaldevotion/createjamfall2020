﻿using ECM.Common;
using ECM.Controllers;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
namespace ECM.Examples
{
    /// <summary>
    /// Headbob animation example:
    ///
    /// This example shows how to create a custom first person controller, this extends the BaseFirstPerson controller
    /// and adds Headbob animation. To do this, we animate the cameraPivot transform, this way we can tailor fit the camera
    /// headbob animation to match our game needs, aditionally, we can use Animation events to trigger sound effects like footsteps, etc.
    /// </summary>

    public class FPS_CustomController : BaseFirstPersonController
    {
        #region EDITOR EXPOSED FIELDS

        [Header("Headbob")]
        public Animator cameraAnimator;

        public List<GameObject> spawnPositions;

        public float cameraAnimSpeed = 1.0f;
        public GameObject visuals;

        #endregion

        #region FIELDS

        private int _verticalParamId;
        private int _horizontalParamId;

        #endregion

        #region METHODS

        /// <summary>
        /// Animate camera pivot to play Headbob like animations, Feed CharacterMovement info to camera animator.
        /// </summary>

        private void AnimateCamera()
        {
            //var normalizedSpeed = Mathf.InverseLerp(0.0f, forwardSpeed, movement.velocity.onlyXZ().magnitude);
            
            var lateralVelocity = Vector3.ProjectOnPlane(movement.velocity, transform.up);
            var normalizedSpeed = Mathf.InverseLerp(0.0f, forwardSpeed, lateralVelocity.magnitude);

            cameraAnimator.speed = Mathf.Max(0.5f, cameraAnimSpeed * normalizedSpeed);

            const float dampTime = 0.1f;

            cameraAnimator.SetFloat(_verticalParamId, moveDirection.z, dampTime, Time.deltaTime);
            cameraAnimator.SetFloat(_horizontalParamId, moveDirection.x, dampTime, Time.deltaTime);
        }

        /// <summary>
        /// Override BaseFirstPersonController Animate method.
        /// </summary>

        protected override void Animate()
        {
            AnimateCamera();
            
        }

        /// <summary>
        /// Override BaseFirstPersonController HandleInput method.
        /// </summary>

        protected override void HandleInput()
        {
            // Add your game custom input code here

            moveDirection = new Vector3
            {
                x = Input.GetAxisRaw("Horizontal"),
                y = 0.0f,
                z = Input.GetAxisRaw("Vertical")
            };

            run = Input.GetButton("Fire3");

            jump = Input.GetButton("Jump");
        }

        /// <summary>
        /// Override BaseFirstPersonController Awake method.
        /// </summary>

        public override void Awake()
        {
            // Initalize BaseFirstPersonController

            base.Awake();

            // Cache animator parameter and state ids

            _verticalParamId = Animator.StringToHash("vertical");
            _horizontalParamId = Animator.StringToHash("horizontal");
            transform.position = new Vector3(Random.Range(-3, 3) , 0.5f, Random.Range(-3, 3) );
            visuals.transform.GetChild(0).gameObject.layer = 30;
            visuals.transform.GetChild(1).gameObject.layer = 30;

        }

        public override void Update()
        {
            base.Update();
            if (hasAuthority)
            {
                if (!Input.GetButton("Fire3"))
                {
                    charAnimator.SetFloat("VelocityZ", Input.GetAxisRaw("Vertical"));
                    charAnimator.SetFloat("VelocityX", Input.GetAxisRaw("Horizontal"));
                }
                else
                {
                    charAnimator.SetFloat("VelocityZ", 2f, 0.2f, Time.fixedDeltaTime);
                    charAnimator.SetFloat("VelocityX", 2f, 0.2f, Time.fixedDeltaTime);
                }
                if (Input.GetKeyDown(KeyCode.PageDown))
                {
                    FindObjectOfType<ShipController>().MainCamera.SetActive(true);

                    if (isServer)
                    {
                        Debug.Log("Hide Rpc");

                        RpcHide();

                    }
                    else
                    {

                        Debug.Log("Hide CMD");

                        CmdHide();

                    }

                }
            }
           
        }
        #endregion
        [Command]
        public void CmdHide()
        {
            if (!isServer) return;
            RpcHide();
        }
        [ClientRpc]
        public void RpcHide()
        {
            gameObject.SetActive(false);
        }

    }
}
