﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Mirror;
using TMPro;
using UnityEngine.UI;
public class ButtonController : NetworkBehaviour
{

    int movLeft;
    int movRight;
    int movUp;
    int movDown;
    public bool EmerButton1;
    public bool EmerButton2;
    public bool EmerButton3;
    public bool EmerButton4;

    public bool isMoving;

    public Vector3 moveDirection;
    public Quaternion moveRotation;

    public ShipController shipController;

    public GameObject textEmerOff1;
    public GameObject textEmerOff2;
    public GameObject textEmerOff3;
    public GameObject textEmerOff4;

    public GameObject textEmerOn1;
    public GameObject textEmerOn2;
    public GameObject textEmerOn3;
    public GameObject textEmerOn4;

    // Start is called before the first frame update
    void Start()
    {
        shipController = GetComponent<ShipController>();
    }

    // Update is called once per frame
    void Update()
    {
       
        
        

    }

    [ClientRpc]
    public void RpcnoMove()
    {
        isMoving = false;
    }
    [Command(ignoreAuthority = true)]

    public void CmdnoMove()
    {
        if (!isServer) return;
        RpcnoMove();
    }

    public void noMove()
    {
        if (isServer)
        {
            RpcnoMove();

        }
        else
        {
            CmdnoMove();

        }
    }
    [Command(ignoreAuthority = true)]

    public void CmdMove(string direction)
    {
        if (!isServer) return;
        Debug.Log("CMD to RPC move");
        RpcMove(direction);
    }
    [ClientRpc]
    public void RpcMove(string direction)
    {
        Debug.Log("Client rpc move");
        switch (direction)
        {
            case "Left":
                Debug.Log("Left, 1");
                moveDirection = new Vector3(moveDirection.x, moveDirection.y, moveDirection.z + 1);
                moveRotation = shipController.initQuat * Quaternion.Euler(new Vector3(0, 0, 5));
                isMoving = true;
                break;
            case "Right":
                Debug.Log("Right");
                moveDirection = new Vector3(moveDirection.x, moveDirection.y, moveDirection.z - 1);
                moveRotation = shipController.initQuat * Quaternion.Euler(new Vector3(0, 0, -5));

                isMoving = true;
                break;
            case "Up":
                Debug.Log("Up, 1");
                moveDirection = new Vector3(moveDirection.x, moveDirection.y + 1, moveDirection.z);
                moveRotation = shipController.initQuat * Quaternion.Euler(new Vector3(5, 0, 0));

                isMoving = true;
                break;
            case "Down":
                Debug.Log("Down");
                moveDirection = new Vector3(moveDirection.x, moveDirection.y -1, moveDirection.z);
                moveRotation = shipController.initQuat * Quaternion.Euler(new Vector3(5, 0, 0));

                isMoving = true;
                break;
            default:
                break;
        }
    }

    public void Left()
    {
        Debug.Log("Left");
        if (isServer)
        {
            Debug.Log("Left Rpc");

            RpcMove("Left");

        }
        else
        {
          
            Debug.Log("Left CMD");

            CmdMove("Left");

        }
    }

    public void Up()
    {
        Debug.Log("UP");

        if (isServer)
        {
            Debug.Log("Up Rpc");

            RpcMove("Up");

        }
        else
        {
      
            Debug.Log("Up CMD");

            CmdMove("Up");

        }
    }

    public void Down()
    {
        Debug.Log("Down");

        if (isServer)
        {
            Debug.Log("Down Rpc");

            RpcMove("Down");

        }
        else
        {
     
            Debug.Log("Down CMD");

            CmdMove("Down");

        }
    }

    public void Right()
    {

        if (isServer)
        {
            Debug.Log("Right Rpc");

            RpcMove("Right");

        }
        else
        {
       
            Debug.Log("Right CMD");

            CmdMove("Right");

        }
    }

    public void presEmerButton1()
    {
        EmerButton1 = true;
        textEmerOff1.SetActive(false);
        textEmerOn1.SetActive(true);

    }

    public void presEmerButton2()
    {
        EmerButton2 = true;
        textEmerOff2.SetActive(false);
        textEmerOn2.SetActive(true);
    }
    public void presEmerButton3()
    {
        EmerButton3 = true;
        textEmerOff3.SetActive(false);
        textEmerOn3.SetActive(true);
    }
    public void presEmerButton4()
    {
        EmerButton4 = true;
        textEmerOff4.SetActive(false);
        textEmerOn4.SetActive(true);
    }

    public void noEmerButton1()
    {
        EmerButton1 = false;
        textEmerOn1.SetActive(false);
        textEmerOff1.SetActive(true);
        
    }
    public void noEmerButton2()
    {
        EmerButton2 = false;
        textEmerOn2.SetActive(false);
        textEmerOff2.SetActive(true);
    }
    public void noEmerButton3()
    {
        EmerButton3 = false;
        textEmerOn3.SetActive(false);
        textEmerOff3.SetActive(true);
    }
    public void noEmerButton4()
    {
        EmerButton4 = false;
        textEmerOn4.SetActive(false);
        textEmerOff4.SetActive(true);
    }


}
