﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class CharInteraction : NetworkBehaviour
{

    public Camera fpsCam;
    public float range = 100f;

    private GameObject button;
    private bool isRayCasting = false;
    private bool hitButton; 
    ShipButton shipButton;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        shipButton = GetComponent<ShipButton>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;


        if (Input.GetButtonDown("Fire1"))
        {
           isRayCasting = true;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            isRayCasting = false;
            if (hitButton)
            {
                animator.SetBool("buttonPressed", false);

                button.GetComponent<ShipButton>().exit.Invoke();
                hitButton = false;
                button = null;
            }
            
        }

        if (isRayCasting)
        {

            RaycastHit hit;
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range) && hit.collider.tag == "Button" )
            {

                if (hit.collider.tag == "Button" && hitButton == false)
                {
                    Debug.Log("Hitting button");
                    hitButton = true;
                    button = hit.collider.gameObject;
                    hit.collider.GetComponent<ShipButton>().buttonPress.Invoke();
                    animator.SetBool("buttonPressed", true);
                }
                return;

            }
            else if (button != null)
            {
                animator.SetBool("buttonPressed", false);

                Debug.Log("No longer hitting button");
                button.GetComponent<ShipButton>().exit.Invoke();
                hitButton = false;
                button = null;
            }
        }

    }


  
}
