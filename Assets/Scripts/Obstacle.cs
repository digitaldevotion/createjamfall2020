﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    int xDead;
    int yDead;
    int zDead;

    ShipController shipCtrl;
    ObstacleHandling ObsHandl;

    // Start is called before the first frame update
    void Start()
    {
         xDead = Random.Range(0, 50);
         yDead = Random.Range(0, 50);
         zDead = Random.Range(0, 50);

        shipCtrl = GameObject.FindObjectOfType<ShipController>();
        ObsHandl = GameObject.FindObjectOfType<ObstacleHandling>();
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.tag == "ObsAlive")
        {

        }
        else if (gameObject.tag == "ObsDead")
        {
            ObstacleDead();
        }
        else if (gameObject.tag == "ObsRotate")
        {
            transform.Rotate(new Vector3(20, 0, 0) * Time.deltaTime);
        }


    }


    void ObstacleDead()
    {

        transform.Rotate(new Vector3(xDead, yDead, zDead) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Spaceship")
        {
            Debug.Log("Crash");
            shipCtrl.Crash();
            GetComponent<BoxCollider>().enabled = false;
            ObsHandl.GetComponent<BoxCollider>().enabled = false;
            Destroy(ObsHandl.gameObject, 2);


        }
    }

}
