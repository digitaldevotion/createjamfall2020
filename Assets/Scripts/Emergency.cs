﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emergency : MonoBehaviour
{
    ButtonController buttonCtrl;
    public bool isEmergency;

    // Start is called before the first frame update
    void Start()
    {
        buttonCtrl = GetComponent<ButtonController>();

    }

    // Update is called once per frame
    void Update()
    {
        if(isEmergency)
        {
            if(buttonCtrl.EmerButton1 && buttonCtrl.EmerButton2 && buttonCtrl.EmerButton3 && buttonCtrl.EmerButton4)
            {
                isEmergency = false;
            }

        }
    }
}
