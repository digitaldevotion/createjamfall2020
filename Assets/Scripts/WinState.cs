﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class WinState : NetworkBehaviour
{

    public GameObject ship;
    public int goalDist;
    public float timeStage = 10;
    public TextMeshPro timeText;
    bool timerIsRunning = false;

    public UnityEvent Win;
    public UnityEvent Loss;


    // Start is called before the first frame update
    void Start()
    {
       // ship.GetComponent<ShipController>().startGame
      //  timerIsRunning = true;
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();


    }
    // Update is called once per frame
    void Update()
    {
        
        if(ship.transform.position.x <= goalDist)
        {
            endGame(true);
            timerIsRunning = false;
        }
        if (ship.GetComponent<ShipController>().startGame)
        {
            if (timeStage > 0)
            {
                timeStage -= Time.deltaTime;
                DisplayTime(timeStage);
            }
            else
            {
                endGame(false);
                Debug.Log("You lost dim dum");
                timeStage = 0;
                ship.GetComponent<ShipController>().startGame = false;
            }

        }
        

    }


    public void endGame(bool didWeWin)
    {
        if (isServer)
        {
            Debug.Log("EndGame Rpc");

            RpcendGame(didWeWin);

        }
        else
        {

            Debug.Log("EndGame CMD");

            CmdendGame(didWeWin);

        }
    }
    [Command]
    public void CmdendGame(bool didWeWin)
    {
        if (!isServer) return;
        Debug.Log("CMD to RPC move");
        RpcendGame(didWeWin);
    }
    [ClientRpc]
    public void RpcendGame(bool didWeWin)
    {
        if (didWeWin)
        {
            timeText.text = "Winners - Reset in 30";
            Invoke("reloadScene", 30f);
            Win.Invoke();
        }
        else
        {
            timeText.text = "Losers - Reset in 30 ";
            Invoke("reloadScene", 30f);
            Loss.Invoke();
        }
    }

    public void reloadScene()
    {
        NetworkManager.singleton.ServerChangeScene(SceneManager.GetActiveScene().name, true);
    }

    [Command]
    public void CmdresetScene()
    {
    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }


}
