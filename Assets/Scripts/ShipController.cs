﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.UI;

public class ShipController : NetworkBehaviour
{
    [SyncVar]
    public Vector3 moveDir;
    [SyncVar]
    public Vector3 rotationAmount;
    [SyncVar]
    public float speed;
    public int maxSpeed;
    public int minSpeed;
    public int boostIncreas;
    public int crashReduction;
    public bool hasColide;
    public Quaternion initQuat;
    public Slider speedMeter;
    public GameObject MainCamera;
    public GameObject crashText;
    public GameObject dodgeText;
    

    ButtonController buttonCtrl;


    private Rigidbody _rigidbody;
    [SyncVar]
    public bool startGame;

    public void Awake()
    {
        
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = true;
        buttonCtrl = GetComponent<ButtonController>();
        Invoke("startTheGame", 5f);

    }

    private void Update()
    {
        speedMeter.value = speed/100;


    }
    public void startTheGame()
    {
        startGame = true;
    }
    public void FixedUpdate()
    {
        if (startGame)
        {
            Quaternion deltaRotation = Quaternion.identity;
            if (buttonCtrl.isMoving)
            {
                moveDir = new Vector3(1,buttonCtrl.moveDirection.y,buttonCtrl.moveDirection.z);
           


            }
            else
            {
                buttonCtrl.moveDirection = Vector3.zero;
                moveDir = new Vector3(1, 0, 0);


            }
            _rigidbody.MovePosition(transform.position+(-moveDir*speed)*Time.fixedDeltaTime);

        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdCrash()
    {
        if (!isServer) return;
        RpcCrash();

    }

    [ClientRpc]
    public void RpcCrash()
    {
        Debug.Log("I pretend to crash");
        if (speed > minSpeed)
        {
            StartCoroutine("CrashTextDisplay");
            speed = speed - crashReduction;
            Debug.Log("I actually crash " + speed);
        }

    }

    public void Crash()
    {
        if (isServer)
        {
            RpcCrash();

        }
        else
        {
            CmdCrash();
        }

    }



    [Command(ignoreAuthority = true)]
    public void CmdBoost()
    {
        if (!isServer) return;
        RpcBoost();

    }

    [ClientRpc]
    public void RpcBoost()
    {
        Debug.Log("I pretend to boost");
        if (speed < maxSpeed)
        {
            StartCoroutine("DodgeTextDisplay");
            speed = speed + boostIncreas;
            Debug.Log("I actually boost " + speed);
        }

    }
    public void Boost()
    {
        if (isServer)
        {
            RpcBoost();

        }
        else
        {
            CmdBoost();
        }
    }


    IEnumerator CrashTextDisplay()
    {
        // suspend execution for 5 seconds
        crashText.SetActive(true);
        yield return new WaitForSeconds(5);
        crashText.SetActive(false);
    }
    IEnumerator DodgeTextDisplay()
    {
        // suspend execution for 5 seconds
        dodgeText.SetActive(true);
        yield return new WaitForSeconds(5);
        dodgeText.SetActive(false);
    }

    //[ClientRpc]
    //public void RpcnoMove()
    //{
    //    isMoving = false;
    //}
    //[Command(ignoreAuthority = true)]

    //public void CmdnoMove()
    //{
    //    if (!isServer) return;
    //    RpcnoMove();
    //}

    //public void noMove()
    //{
    //    if (isServer)
    //    {
    //        RpcnoMove();

    //    }
    //    else
    //    {
    //        CmdnoMove();

    //    }
    //}
}



