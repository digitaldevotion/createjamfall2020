﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class ObstacleHandling : NetworkBehaviour
{

    public GameObject deadPlane;
    public GameObject alivePlane;
    public GameObject meteor;
    public GameObject alienProbe;
    public GameObject astroid;

    public List<GameObject> obstacleList = new List<GameObject>();

    SpawnPointHandler spawnPointHandler;
    ShipController shipCtrl;

    // Start is called before the first frame update
    void Start()
    {
        spawnPointHandler = GameObject.FindObjectOfType<SpawnPointHandler>();
        shipCtrl = spawnPointHandler.shipCtrl;
        //Add all Obstacle objects to a list. 
        obstacleList.Add(deadPlane);
        obstacleList.Add(alivePlane);
        obstacleList.Add(meteor);
        obstacleList.Add(alienProbe);
        obstacleList.Add(astroid);
        
        SpawnObstacle();
    }

    // Update is called once per frame
    void Update()
    {
     
    }
    
    void SpawnObstacle()
    {
        //randomely select a gameObject from the list, based on the length of the list. 
        GameObject obstaclePrefab = obstacleList[Random.Range(0, obstacleList.Count)];
        // select a number between 0-9. (It is exclusive the last number) 
        int obstacleSpawnIndex = Random.Range(0, 10);
        // Select one of the nine spawn locations
        Transform spawnPoint = transform.GetChild(obstacleSpawnIndex).transform;
        //offset the spawnPoint randomely
        spawnPoint.transform.position += new Vector3(0, Random.Range(0, 50), Random.Range(0, 50));
        //Create the Prefab in the chosen spawnPoint. 
        Instantiate(obstaclePrefab, spawnPoint.position, Quaternion.identity, transform);
        
              

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Spaceship")
        {
            shipCtrl.Boost();
            Debug.Log("Collider has been triggered + Boost or Crash");
            spawnPointHandler.SpawnSpawners();
            Destroy(gameObject, 2);
            
        }
       
    }

}
