﻿using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;


public class NetworkRoomPlayerPT : NetworkBehaviour
{
    [Header("UI")]
    [SerializeField] private GameObject lobbyUI = null;
    [SerializeField] private TMP_Text[] playerNameTexts = new TMP_Text[4];
    [SerializeField] private TMP_Text[] playerReadyTexts = new TMP_Text[4];
    [SerializeField] private Button startGameButton = null;
    [SerializeField] private Image keyboardAsController = null;
    [SerializeField] private Image joystickAsController = null;
    [SerializeField] private TMP_Text noJoysticksAvailable = null;
    [SerializeField] private TMP_Text textAmountOfConnections = null;
    
    [SyncVar(hook = nameof(HandleDisplayNameChanges))]
    public string DisplayName = "Loading...";
    [SyncVar(hook = nameof(HandleReadyStatusChanged))]
    public bool IsReady = false;
    [SyncVar(hook = nameof(HandleAmountOfConnsChanged))]
    public int amountOfConnections;

    private bool isLeader;
    public bool IsLeader
    {
        set
        {
            isLeader = value;
            startGameButton.gameObject.SetActive(value);
        }
    }

    bool keyboardIsControllerType = true;

    private NetworkManagerPT room;
    private NetworkManagerPT Room
    {
        get
        {
            if (room != null) { return room; }
            return room = NetworkManager.singleton as NetworkManagerPT;
        }
    }

    public override void OnStartAuthority()
    {
        CmdSetDisplayName(PlayerNameInput.DisplayName);

        lobbyUI.SetActive(true);
    }

    public override void OnStartClient()
    {
        Room.RoomPlayers.Add(this);

        Room.UpdateAmountOfConnections();

        UpdateDisplay();


    }
    public override void OnStopClient()
    {
        base.OnStopClient();
        Room.RoomPlayers.Remove(this);

        UpdateDisplay();
    }


    public void HandleReadyStatusChanged(bool oldValue, bool newValue) => UpdateDisplay();
    public void HandleDisplayNameChanges(string oldValue, string newValue) => UpdateDisplay();
    public void HandleAmountOfConnsChanged(int oldValue, int newValue) => UpdateDisplay();

    private void UpdateDisplay()
    {
        if (!hasAuthority)
        {
            foreach (var player in Room.RoomPlayers)
            {
                if (player.hasAuthority)
                {
                    player.UpdateDisplay();
                    break;
                }
            }

            return;
        }

        // for (int i = 0; i < playerNameTexts.Length; i++)
        // {
        //     playerNameTexts[i].text = "Waiting For Player...";
        //     playerReadyTexts[i].text = string.Empty;
        // }

        // for (int i = 0; i < Room.RoomPlayers.Count; i++)
        // {
        //     playerNameTexts[i].text = Room.RoomPlayers[i].DisplayName;
        //     playerReadyTexts[i].text = Room.RoomPlayers[i].IsReady ?
        //         "<color=green>Ready</color>" :
        //         "<color=red>Not Ready</color>";
        // }
        textAmountOfConnections.text = amountOfConnections.ToString();
    }

    public void UpdateAmountOfConnections(int amount)
    {
        amountOfConnections = amount;
    }

    public void HandleReadyToStart(bool readyToStart)
    {
        if (!isLeader) { return; }

        startGameButton.interactable = readyToStart;
    }

    [Command]
    private void CmdSetDisplayName(string displayName)
    {
        DisplayName = displayName;
    }

    [Command]
    public void CmdReadyUp()
    {
        IsReady = !IsReady;

        Room.NotifyPlayersOfReadyState();
    }

    [Command]
    public void CmdStartGame()
    {
        if (Room.RoomPlayers[0].connectionToClient != connectionToClient) { return; }

        Room.StartGame();
    }

    //public void SetKeyboardAsController()
    //{
    //    if (keyboardIsControllerType)
    //    {
    //        return;
    //    }
    //    keyboardIsControllerType = true;
    //    keyboardAsController.enabled = true;
    //    joystickAsController.enabled = false;
    //}

    //public void SetJoystickAsController()
    //{
    //    if (!keyboardIsControllerType)
    //    {
    //        return;
    //    }
    //    if (!Room.controllerManager.CheckIfAnyJoystickIsAvailable())
    //    {
    //        StartCoroutine(NoJoysticksAvailable());
    //        return;
    //    }
    //    keyboardIsControllerType = false;
    //    keyboardAsController.enabled = false;
    //    joystickAsController.enabled = true;
    //}

    IEnumerator NoJoysticksAvailable()
    {
        noJoysticksAvailable.enabled = true;
        yield return new WaitForSeconds(2f);
        noJoysticksAvailable.enabled = false;
    }

    public bool GetControllerType()
    {
        return keyboardIsControllerType;
    }
}
