﻿using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;


public class NetworkGamePlayerPT : NetworkBehaviour
{


    //[SyncVar(hook = nameof(SetIngameDisplayName))]
    //private string displayName = "Loading...";

    //[SerializeField]
    //private bool keyboardIsControllerType;
    ////[SerializeField] TMP_Text ingameDisplayNameText = null;

    //[SerializeField]
    //public List<Material> playerMaterials;
    //[SerializeField]
    //public SkinnedMeshRenderer playerMesh;
    private NetworkManagerPT room;
    private NetworkManagerPT Room
    {
        get
        {
            if (room != null) { return room; }
            return room = NetworkManager.singleton as NetworkManagerPT;
        }
    }

    
    public override void OnStartClient()
    {
        DontDestroyOnLoad(gameObject);

        Room.GamePlayers.Add(this);

    }

    public override void OnStartAuthority()
    {

    }
    
    //public override void OnNetworkDestroy()
    //{
    //    Room.GamePlayers.Remove(this);
    //}


    [Server]
    public void SetDisplayName(string displayName)
    {
      //  this.displayName = displayName;
        //SetIngameDisplayName();
    }

    //public string GetDisplayName()
    //{
    // //   return this.displayName;
    //}

    //public void SetControllerType(bool value)
    //{
    //   // this.keyboardIsControllerType = value;
    //}

    //public bool GetControllerType()
    //{
    //   // return this.keyboardIsControllerType;
    //}

    //public bool PlayerIsUsingKeyboard()
    //{
    // //   return this.keyboardIsControllerType;
    //}


    //public void SetIngameDisplayName(string oldValue, string newValue)
    //{
    //   // ingameDisplayNameText.text = displayName;
    //}

    //[Command]
    //void CmdSetDisplayName(string displayName)
    //{
    //   // this.displayName = displayName;
    //}

//    [Command]
//    void CmdSetControllerType(bool value)
//    {
//   7/     this.keyboardIsControllerType = value;
//    }
}
