﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class twoDimensionalAnimationStateController : MonoBehaviour
{
    Animator animator;
    float velocityZ = 0.0f;
    float velocityX = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //bool forwardPressed = Input.GetKey("w");
        //bool leftPressed = Input.GetKey("a");
        //bool backwardPressed = Input.GetKey("s");
        //bool rightPressed = Input.GetKey("d");
        //bool interactPressed = Input.GetKey("e");
        //bool runPressed = Input.GetKey("left shift");

        //if (forwardPressed)
        //{
        //    velocityZ += Time.deltaTime * acceleration;
        //}
        
        //if (backwardPressed)
        //{
        //    velocityZ -= Time.deltaTime * acceleration;
        //}

        //if (leftPressed)
        //{
        //    velocityX -= Time.deltaTime * acceleration;
        //}

        //if (rightPressed)
        //{
        //    velocityX += Time.deltaTime * acceleration;
        //}

        animator.SetFloat("Velocity Z", velocityZ);
        animator.SetFloat("Velocity X", velocityX);

    }
}
